<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $employees = Employee::latest()->paginate(5);

        return view('employees.index', compact('employees'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $req)
    {
        return view('employees.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $req)
    {
        //
        $fileName ="";
        $filePath="";
        $someJSON="";
        //checking the file;
         if($req->file()) {
                $fileName = time().'_'.$req->file->getClientOriginalName();
                $filePath = $req->file('file')->storeAs('uploads', $fileName, 'public');
            
            //taking the path
            $path = storage_path('app/public/uploads/'.$fileName);;// Storage::disk('public')->path($fileName);
            
            // extract the content from the saved file
            $content = json_decode(file_get_contents($path), true);

            $var = "";
            $employees=array();
            foreach($content as $key => $value){ // iterating through the array
                $var =  array_flip($value);//flip the array in order to make the employee name thekey
                array_push($employees,$var);
            }
            //convert the array to a collection
            $collection = collect($employees);
           //group the resulted 
            $grouped = $collection->mapToGroups(function ($item, $key) {
                return $item;
            });
            $grouped->all();
            //return the result as json
            $someJSON = json_encode($grouped);
        }
        return redirect()->route('employees.index')
            ->with('success',  $someJSON == ""? "" :$someJSON);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(employee $employee)
    {
        //
        return view('employee.show', compact('employee'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(employee $employee)
    {
        //
        return view('employee.edit', compact('employee'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, employee $employee)
    {
        //

        $request->validate([
            'employeeName' => 'required',
            'file' => 'required'
        ]);
        $employee->update($request->all());

        return redirect()->route('emplyoyee.index')
            ->with('success', 'employee updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(employee $employee)
    {
        //
        $project->delete();

        return redirect()->route('projects.index')
            ->with('success', 'Project deleted successfully');
    }
}
