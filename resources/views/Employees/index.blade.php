@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('employees.store') }}" title="Create a employee"> <i class="fas fa-plus-circle"></i>
                    </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <form action="{{route('employees.store')}}" method="post" enctype="multipart/form-data">
    @csrf

        <div class="row">
            <div class="col-lg-3">
                <div class="form-group">
                <label for="exampleFormControlFile1">please insert Employeedocs</label>
                <input type="file" class="form-control-file" name="file" id="file">
                </div>
            </div> 
            <div class="col-lg-2">
                <button type="submit" name="submit" class="btn btn-primary btn-block mt-4">
                    Upload Files
                </button>
            </div>
        </div>

      
    </form>
    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>EmployeeName</th>
            <th>File</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($employees as $employee)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $employee->employeeName }}</td>
                <td>{{ $employee->file }}</td>
                <td>
                    <form action="{{ route('employees.destroy', $employee->id) }}" method="POST">

                        <a href="{{ route('employees.show', $employee->id) }}" title="show">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>

                        <a href="{{ route('employees.edit', $employee->id) }}">
                            <i class="fas fa-edit  fa-lg"></i>

                        </a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                            <i class="fas fa-trash fa-lg text-danger"></i>

                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $employees->links() !!}

@endsection
